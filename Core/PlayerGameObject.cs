﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Haggle.Core {
	public class PlayerGameObject: Collider, IGameObject {
		private Texture2D _sprite;
		private Vector2 _position = new Vector2(160, 120);
		private int speed = 2;
		private Rectangle collisionArea;

		public PlayerGameObject() {
		}

		public void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
			spriteBatch.Draw(_sprite, _position);

			var bounds = collisionArea;
			bounds.Offset(_position);
			Debug.DrawRectangle(bounds, spriteBatch);
		}

		public void LoadContent(ContentManager content) {
			_sprite = content.Load<Texture2D>("MC");

			collisionArea = _sprite.Bounds;
			collisionArea.Inflate(-4, -6);
		}

		public void UnloadContent() {
			throw new NotImplementedException();
		}

		public void Update(GameTime gameTime) {
			var keyboardState = Keyboard.GetState();

			// Handle input
			if(keyboardState.IsKeyDown(Keys.Up))
				_position.Y -= speed;
			if(keyboardState.IsKeyDown(Keys.Down))
				_position.Y += speed;
			if(keyboardState.IsKeyDown(Keys.Left))
				_position.X -= speed;
			if(keyboardState.IsKeyDown(Keys.Right))
				_position.X += speed;

			Rectangle playerArea = Game1.GetPlayerArea();
			_position.X = MathHelper.Clamp(
				_position.X, playerArea.Left, playerArea.Right - _sprite.Width);
			_position.Y = MathHelper.Clamp(
				_position.Y, playerArea.Top, playerArea.Bottom - _sprite.Height);
		}

		public override Rectangle GetCollisionArea() {
			var bounds = collisionArea;
			bounds.Offset(_position.ToPoint());
			return bounds;
		}

		public override Int32 GetDamage() {
			return 0;
		}
	}
}
