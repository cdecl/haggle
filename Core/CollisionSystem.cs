﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Haggle.Core {
	public class CollisionSystem {
		private readonly List<ICollider> colliders;
		private ICollider playerCollider;

		private readonly List<ICollider> collidersToRemove;

		private event CollisionHandler handler;

		public void RegisterHandler(CollisionHandler handler) {
			this.handler += handler;
		}

		public IEnumerable<ICollider> Colliders {
			get { return colliders; }
		}

		public CollisionSystem() {
			colliders = new List<ICollider>();
			collidersToRemove = new List<ICollider>();
		}

		public void RegisterCollider(ICollider collider) {
			colliders.Add(collider);
		}

		public void UnregisterCollider(ICollider collider) {
			collidersToRemove.Add(collider);
		}

		public void Run() {
			foreach(var collider in colliders) {
				if(playerCollider.GetCollisionArea()
					 .Intersects(collider.GetCollisionArea())) {
					// Trigger handlers registered to the player
					playerCollider.DidCollide(playerCollider, collider);
					// Trigger handlers registered on the colliding object
					collider.DidCollide(playerCollider, collider);
					// Trigger handlers registerd for all collisions
					if(handler != null)
						handler(playerCollider, collider);
				}
			}

			colliders.RemoveAll((obj) => collidersToRemove.Contains(obj));
			collidersToRemove.Clear();
		}

		public void SetPlayerCollider(ICollider collider) {
			playerCollider = collider;
		}
	}
}
