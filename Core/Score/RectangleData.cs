﻿using System;
using Microsoft.Xna.Framework;
namespace Haggle.Core {

	/// <summary>
	/// Cans the spaghetti.
	/// </summary>
	class RectangleData {
		public Color[] data { get { return _data; } }
		Color[] _data;
		Color boxColour;

		public RectangleData(Color boxColour, Rectangle area,
		                     int paddingX, int paddingY, Color? defaultColour = null) {
			// ⚠ CAUTION ⚠ watch out for spilt spaghetti. 
			// sets the pixels in the data array to be a rectangle with 2-pixel width
			// lines, of half the score area width minus the padding.
			this.boxColour = boxColour;
			int width = area.Width;
			if(defaultColour == null)
				defaultColour = Color.Transparent;
			_data = new Color[width * area.Height];
			for(int i = 0; i < _data.Length; i++)
				_data[i] = defaultColour.Value;
			// top line
				for(int i = paddingY * width + paddingX; i < paddingY * width + width - paddingX; ++i) {
				_data[i] = boxColour;
				_data[i + width] = boxColour;
			}
			// side lines.
			for(int i = paddingY * width + paddingX; i < _data.Length - paddingY * width; i += width) {
				_data[i] = boxColour;
				_data[i + 1] = boxColour;
				_data[i + width - 2 - 2 * paddingX] = boxColour;
				_data[i + width - 1 - 2 * paddingX] = boxColour;
			}
			// bottom line.
			for(int i = _data.Length - width * (paddingY + 1) + paddingX;
					i < _data.Length - width * paddingY - paddingX; ++i) {
				_data[i] = boxColour;
				_data[i - width] = boxColour;
			}
		}
	}
}
