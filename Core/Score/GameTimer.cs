﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
namespace Haggle.Core {
	public class GameTimer : IGameObject{
		private Texture2D clock;
		private Vector2 clockPosition, clockScale, textPosition;
		private RectangleData data;
		private Rectangle area;
		private SpriteFont font;
		private Color textColour;
		private TimeSpan start, elapsedTime;
		private bool hasStarted, isStopped;

		public GameTimer() {
			int paddingX = 6;     
			// left area is the left half of the scoreArea
			area = Game1.GetScoreArea();
			area.Width = area.Width / 2 + paddingX / 2;
			data = new RectangleData(Color.Chocolate, area, paddingX, 4);
			clockPosition = new Vector2(area.Left + 18, area.Top + 8);
			clockScale = new Vector2(1.2f, 1.2f);
			textPosition = new Vector2(clockPosition.X + 40, clockPosition.Y + 7);
			textColour = Color.Pink;
			hasStarted = false;
			isStopped = false;
		}

		public void stop() {
			isStopped = true;
		}

		public void Update(GameTime gameTime) {
			if(!isStopped) {
				if(!hasStarted) { // only init start once.
					start = gameTime.TotalGameTime;
					hasStarted = true;
				}
				elapsedTime = (gameTime.TotalGameTime - start);
			}
		}

		public void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
			// Left rectangle.
			Texture2D rect = new Texture2D(spriteBatch.GraphicsDevice, area.Width, area.Height);
			rect.SetData(data.data);
			Vector2 coor = new Vector2(area.Right - area.Width, area.Top); 
			spriteBatch.Draw(rect, coor);

			// Draw clock.
			spriteBatch.Draw(clock, clockPosition, scale: clockScale);
			spriteBatch.DrawString(font, 
			                       String.Format("{0:00}:{1:00}", 
			                                     elapsedTime.Minutes, 
			                                     elapsedTime.Seconds), 
			                       textPosition, 
			                       textColour);
		}

		public void LoadContent(ContentManager content) {
			clock = content.Load<Texture2D>("clock");
			font = content.Load<SpriteFont>("Fonts/ScoreFont");
		}

		public void UnloadContent() {
			throw new NotImplementedException();
		}
	}
}
