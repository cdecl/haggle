﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Haggle.Core {
	public class FinalScore : IGameObject {
		private RectangleData data;
		private Rectangle rect;
		private SpriteFont font;
		private Vector2 textPos, scorePos, losePos;
		private int score;

		public FinalScore(GameScore gameScore) {
			rect = new Rectangle(95, 70, 160, 80);
			data = new RectangleData(Color.Chocolate, rect, 0, 0, defaultColour: Color.Black);
			this.score = Math.Max(gameScore.getScore(), 0);
			int Y = rect.Center.Y - 14;
			if(this.score == 0)
				Y -= 10;

			textPos = new Vector2(rect.Center.X - 30, Y);
			scorePos = new Vector2(textPos.X, textPos.Y + 15);
			losePos = new Vector2(textPos.X - 40, scorePos.Y + 15);
		}

		public void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
			Texture2D rectangle = new Texture2D(spriteBatch.GraphicsDevice, rect.Width, rect.Height);
			rectangle.SetData(data.data);
			spriteBatch.Draw(rectangle, new Vector2(rect.X, rect.Y), layerDepth: .8f);

			// Text
			Vector2 scoreWidth = font.MeasureString("$" + score);
			Vector2 textWidth = font.MeasureString("Final Price:");
			scorePos.X = (int)(textPos.X + (textWidth.X - scoreWidth.X) / 2); //"$"+score
			spriteBatch.DrawString(font, "Final Price:", textPos, Aesthetics.TextColour, 
			                       0, default(Vector2), 1, default(SpriteEffects), .9f);
			spriteBatch.DrawString(font, "$" + score, scorePos, Aesthetics.TextColour,
														 0, default(Vector2), 1, default(SpriteEffects), .9f);
			if(score == 0)
				spriteBatch.DrawString(font, "Too bad, you've been robbed!",
				                       losePos, Aesthetics.TextColour, 0, 
				                       default(Vector2), 1, default(SpriteEffects), .9f);
		}

		public void LoadContent(ContentManager content) {
			font = content.Load<SpriteFont>("Fonts/ScoreFont");
		}

		public void UnloadContent() {
		}

		public void Update(GameTime gameTime) {
		}
	}
}
