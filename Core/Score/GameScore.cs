﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Haggle.Core {
	public class GameScore: IGameObject {
		private Texture2D goldSprite;
		private Vector2 goldPosition, goldScale, textPosition, scorePosition;
		private RectangleData data;
		private Rectangle area;
		private SpriteFont font;
		private int score;
		private TimeSpan lastUpdate;
		private Game1 game;
		private bool isStopped;

		public GameScore(Game1 game) {
			//Square
			int paddingX = 6; // to avoid having the padding twice in the middle.
												// rightArea is the right half of the scoreArea.
			area = Game1.GetScoreArea();
			area.Width = area.Width / 2 + paddingX / 2;
			area.X += area.Width - paddingX;
			data = new RectangleData(Color.Chocolate, area, paddingX, 4);

			goldPosition = new Vector2(area.Left + 26, area.Top + 10);
			goldScale = new Vector2(.65f, .65f);
			textPosition = new Vector2(area.Left + 55, area.Top + 10);
			scorePosition = new Vector2(textPosition.X, textPosition.Y + 12);
			game.collisionSystem.RegisterHandler(collisionHandler);
			lastUpdate = new TimeSpan();
			this.game = game;
			this.isStopped = false;
		}

		public int getScore() {
			return score;
		}

		public void stop() {
			score = Math.Max(score, 0);
			isStopped = true;
		}

		public void Update(GameTime gameTime) {
			if(!isStopped) {
				// Score increases faster as time increases. 
				// Rate should increase by 10 every 10 seconds.
				if((gameTime.TotalGameTime - lastUpdate).Milliseconds >= 100) {
					score += 10 + 50 * (int)(gameTime.TotalGameTime.Seconds / 20);
					lastUpdate = gameTime.TotalGameTime;
				}
			}
		}

		public void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
			// Right rectangle.
			Texture2D rect = new Texture2D(spriteBatch.GraphicsDevice, area.Width, area.Height);
			rect.SetData(data.data);
			Vector2 coor = new Vector2(area.Right - area.Width, area.Top);
			spriteBatch.Draw(rect, coor);

			// Draw moneybag
			spriteBatch.Draw(goldSprite, goldPosition, scale: goldScale);
			spriteBatch.DrawString(font, "Coins", textPosition, Aesthetics.TextColour);
			spriteBatch.DrawString(font,
														 string.Format("{0:00000}", score),
														 scorePosition,
			                       Aesthetics.TextColour);
		}

		public void LoadContent(ContentManager content) {
			goldSprite = content.Load<Texture2D>("money_bag");
			font = content.Load<SpriteFont>("Fonts/ScoreFont");
		}

		public void UnloadContent() {
			throw new NotImplementedException();
		}

		private void collisionHandler(ICollider player, ICollider collider) {
			if(!isStopped) {
				Console.WriteLine("collision occurred!");
				score -= collider.GetDamage();
				if(score <= 0)
					game.endBattle();
			}
		}
	}
}
