﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Haggle.Core {
	/// <summary>
	/// Responsible for drawing minor aesthetic components.
	/// </summary>
	public class Aesthetics : IGameObject {
		public static readonly Color TextColour = Color.Pink;

		private SpriteFont font;
		private Vector2 dealTextPos;
		private TimeSpan lastUpdate, visibleStart;
		private int blinkDuration;
		private bool textVisible;

		public Aesthetics() {
			Rectangle enemyArea = Game1.GetEnemyArea();
			dealTextPos = new Vector2(enemyArea.Center.X - 45, enemyArea.Center.Y + 5);
			blinkDuration = 600;
		}

		public void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
			if((gameTime.TotalGameTime - lastUpdate).Milliseconds > blinkDuration
			   && gameTime.TotalGameTime.Seconds < 16) {
				textVisible = true;
				visibleStart = gameTime.TotalGameTime;
			}

			if(textVisible) {
				spriteBatch.DrawString(font, "Press D to strike a Deal!", dealTextPos, TextColour);
				lastUpdate = gameTime.TotalGameTime;
				if((gameTime.TotalGameTime - visibleStart).Milliseconds > blinkDuration)
					textVisible = false;
			}
		}

		public void LoadContent(ContentManager content) {
			font = content.Load<SpriteFont>("Fonts/ScoreFont");
		}

		public void UnloadContent() {
		}

		public void Update(GameTime gameTime) {
		}
	}
}
