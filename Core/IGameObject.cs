﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Haggle.Core {
	public interface IGameObject {
		/// <summary>
		/// Render the game object
		/// </summary>
		void Draw(GameTime gameTime, SpriteBatch spriteBatch);

		/// <summary>
		/// Run game object logic
		/// </summary>
		void Update(GameTime gameTime);

		/// <summary>
		/// Load game object content
		/// </summary>
		void LoadContent(ContentManager content);

		/// <summary>
		/// Unload game object content
		/// </summary>
		void UnloadContent();
	}
}
