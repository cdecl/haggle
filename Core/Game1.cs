﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.Collections.Concurrent;
using Autofac;

namespace Haggle.Core {
	/// <summary>
	/// This is the main type for your game.
	/// </summary>
	public class Game1: Game {
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		RenderTarget2D scene;

		private Texture2D _background;
		IGameObject slimeDrop;
		PlayerGameObject player;

		private List<IGameObject> _gameObjects, _buffer, _toRemove;

		public static UInt16 Width = 320;
		public static UInt16 Height = 240;
		public static UInt16 PlayerAreaWidth = 138;
		public static UInt16 PlayerAreaY = 35;

		public CollisionSystem collisionSystem;

		private IContainer container;

		public Game1() {
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize() {
			// TODO: Add your initialization logic here
			_gameObjects = new List<IGameObject>();
			_buffer = new List<IGameObject>();
			_toRemove = new List<IGameObject>();
			player = new PlayerGameObject();
			RegisterGameObject(player);
			scene = new RenderTarget2D(graphics.GraphicsDevice,
																 Width,
																 Height,
																 false,
																 SurfaceFormat.Color,
																 DepthFormat.None,
																 1,
																 RenderTargetUsage.DiscardContents);

			collisionSystem = new CollisionSystem();
			collisionSystem.SetPlayerCollider(player);

			// Dependency injection
			var builder = new ContainerBuilder();

			// Register individual components
			/*builder.RegisterInstance(new TaskRepository())
						 .As<ITaskRepository>();
			builder.RegisterType<TaskController>();
			builder.Register(c => new LogManager(DateTime.Now))
						 .As<ILogger>();

			// Scan an assembly for components
			builder.RegisterAssemblyTypes(myAssembly)
						 .Where(t => t.Name.EndsWith("Repository"))
						 .AsImplementedInterfaces();*/

			builder.RegisterInstance(this).AsSelf();
			builder.RegisterType<Slime>().AsSelf();
			builder.RegisterType<GameScore>().SingleInstance();
			builder.RegisterType<GameTimer>().SingleInstance();
			builder.RegisterType<Aesthetics>().SingleInstance();
			builder.RegisterType<FinalScore>().AsSelf();

			container = builder.Build();

			// Register objects.
			RegisterGameObject(container.Resolve<GameScore>());
			RegisterGameObject(container.Resolve<GameTimer>());
			RegisterGameObject(container.Resolve<Aesthetics>());
			RegisterGameObject(container.Resolve<Slime>());

			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent() {
			_gameObjects.AddRange(_buffer);
			_buffer.Clear();

			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch(GraphicsDevice);

			//TODO: use this.Content to load your game content here 
			_background = Content.Load<Texture2D>("BackgroundWithStars");

			foreach(IGameObject gameObject in _gameObjects)
				gameObject.LoadContent(Content);

			//collisionSystem.SetPlayerCollider(player);
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime) {
			if(Keyboard.GetState().IsKeyDown(Keys.D))
				endBattle();

			_gameObjects.RemoveAll((obj) => _toRemove.Contains(obj));
			_toRemove.Clear();

			_gameObjects.AddRange(_buffer);
			_buffer.Clear();

			// TODO: Add your update logic here
			collisionSystem.Run();

			foreach(IGameObject gameObject in _gameObjects)
				gameObject.Update(gameTime);

			//collisionSystem.SetPlayerCollider(player);

			base.Update(gameTime);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime) {
			GraphicsDevice.SetRenderTarget(scene);
			graphics.GraphicsDevice.Clear(Color.CornflowerBlue);

			// draw your game NES mode: Additive
			spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.NonPremultiplied);
			spriteBatch.Draw(_background, new Vector2(0, 0));
			foreach(IGameObject gameObject in _gameObjects)
				gameObject.Draw(gameTime, spriteBatch);
			spriteBatch.End();

			GraphicsDevice.SetRenderTarget(null);

			float outputAspect = Window.ClientBounds.Width / (float)Window.ClientBounds.Height;
			float preferredAspect = (float)Width / Height;

			Rectangle dst;
			if(outputAspect <= preferredAspect) {
				// output is taller than it is wider, bars on top/bottom
				int presentHeight = (int)((Window.ClientBounds.Width / preferredAspect) + 0.5f);
				int barHeight = (Window.ClientBounds.Height - presentHeight) / 2;
				dst = new Rectangle(0, barHeight, Window.ClientBounds.Width, presentHeight);
			} else {
				// output is wider than it is tall, bars left/right
				int presentWidth = (int)((Window.ClientBounds.Height * preferredAspect) + 0.5f);
				int barWidth = (Window.ClientBounds.Width - presentWidth) / 2;
				dst = new Rectangle(barWidth, 0, presentWidth, Window.ClientBounds.Height);
			}

			graphics.GraphicsDevice.Clear(ClearOptions.Target, Color.Black, 1.0f, 0);
			spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Opaque);
			spriteBatch.Draw(scene, dst, Color.White);
			spriteBatch.End();

			base.Draw(gameTime);
		}

		public void RegisterGameObject(IGameObject gameObject) {
			// add to buffer to avoid modifying list during enumeration.
			_buffer.Add(gameObject);
			gameObject.LoadContent(Content);
		}

		public void UnregisterGameObject(IGameObject gameObject) {
			_toRemove.Add(gameObject);
		}

		public void endBattle() {
			// Stop the game.
			container.Resolve<GameScore>().stop();
			container.Resolve<GameTimer>().stop();
			// Display result.
			RegisterGameObject(container.Resolve<FinalScore>());
		}

		public static Rectangle GetPlayerArea() {
			return new Rectangle(Width - PlayerAreaWidth,
													 PlayerAreaY,
													 PlayerAreaWidth, Height - PlayerAreaY - 60);
		}

		public static Rectangle GetEnemyArea() {
			return new Rectangle(0, 0, Width - PlayerAreaWidth, Height);
		}

		public static Rectangle GetScoreArea() {
			return new Rectangle(45, 183, 230, 44);
		}
	}
}