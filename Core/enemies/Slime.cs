﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
namespace Haggle.Core {
	public class Slime: IGameObject {
		public ProjectileForge forge { get; }
		private Wind wind;
		private Vector2 position, bounce;
		private Texture2D sprite;
		private TimeSpan LastUpdated, LastSlime;
		private Game1 game;
		private bool WindStarted;

		public Slime(Game1 game) {
			this.game = game;
			wind = new Wind(SlimeDrop.Speed);
			forge = new SlimeProjectileForge(game, wind);
			Rectangle enemyPos = Game1.GetEnemyArea();
			position = new Vector2(enemyPos.Width / 2, enemyPos.Height / 2);
			bounce = new Vector2(0, 2);
			//TODO: Clean up Spaghetti
			game.RegisterGameObject(wind);
			game.RegisterGameObject(this);
			WindStarted = false;
		}

		public void Update(GameTime gameTime) {
			if((gameTime.TotalGameTime - LastUpdated).Milliseconds > 230) {
				LastUpdated = gameTime.TotalGameTime;
				position += bounce;
				bounce = -bounce;
			}

			if((gameTime.TotalGameTime - LastSlime).Milliseconds > 400) {
				LastSlime = gameTime.TotalGameTime;
				game.RegisterGameObject(forge.spawn_projectile(gameTime));
			}

			if(!WindStarted && gameTime.TotalGameTime.Seconds > 20) {
				wind.startWind(gameTime);
				WindStarted = true;
			}
		}

		public void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
			spriteBatch.Draw(sprite, position);
		}

		public void LoadContent(ContentManager content) {
			sprite = content.Load<Texture2D>("Slime");
			position.Y -= sprite.Height / 2;
		}

		public void UnloadContent() {
			throw new NotImplementedException();
		}
	}

	/// <summary>
	/// Models the wind aspect of the slime monster.
	/// </summary>
	public class Wind: IGameObject {
		public Vector2 force { get; set; }
		private Probability direction;
		private Texture2D sprite;
		private Rectangle source;
		private int state;
		private const int num_states = 50;
		private TimeSpan last_updated;
		private Vector2 position;
		private float speed;
		private bool start;
		private TimeSpan startTime;

		public Wind(float speed) {
			Rectangle playerPos = Game1.GetPlayerArea();
			position = new Vector2(playerPos.Left + playerPos.Width / 2, playerPos.Center.Y);
			force = new Vector2(0, 0);
			last_updated = new TimeSpan(0);
			this.speed = speed;
			state = 1; // used for animation.
			start = false; // prevents the distributions from updating
										 // used to determine if the direction changes.0 == no change, 1 == change.
			direction = Probability.get_time_skewed(2, 1);
		}

		/// <summary>
		/// Transitions from no wind, to wind.
		/// </summary>
		public void startWind(GameTime gameTime) {
			force = new Vector2(speed, 0);
			start = true;
			// Used to calculate time since wind started.
			startTime = gameTime.TotalGameTime;
		}

		public void Update(GameTime gameTime) {
			if(start && (gameTime.TotalGameTime - last_updated).Milliseconds > 20) {
				last_updated = gameTime.TotalGameTime;
				if(state == num_states) { // only change direction when animation done.
																	// if more difficult RV value, change direction.
					if(direction.sample_distribution(
						(gameTime.TotalGameTime - startTime).Seconds / 30) == 1) {
						force = new Vector2(-force.X, force.Y);
					}
					state = 0;
				} else
					state++;
			}
		}

		public void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
			// if force.X is positive, wind is east.
			// TODO: Clean up the spilt spaghetti. (Using actual Sprites)
			// At least it's canned spaghetti?
			if(force.X > 0) {
				source = new Rectangle(0, 0,
														 (int)(((float)sprite.Width / num_states) * state),
														 sprite.Height);
				spriteBatch.Draw(sprite, position, sourceRectangle: source);
			} else if(force.X < 0) {
				source = new Rectangle(0, 0,
														(int)(((float)sprite.Width / num_states) * state),
														 sprite.Height);
				spriteBatch.Draw(sprite, position + new Vector2(sprite.Width - source.Width, 0),
												 sourceRectangle: source,
												 effects: SpriteEffects.FlipHorizontally);
			}
		}

		public void LoadContent(ContentManager content) {
			sprite = content.Load<Texture2D>("Wind");
			position.X -= sprite.Width / 2; // offset position by sprite size.
		}

		public void UnloadContent() {
			throw new NotImplementedException();
		}
	}
}
