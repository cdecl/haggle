﻿using Microsoft.Xna.Framework;
using System;

namespace Haggle.Core {
	public delegate void CollisionHandler(ICollider player, ICollider collider);

	public interface ICollider {
		Rectangle GetCollisionArea();

		void RegisterHandler(CollisionHandler handler);

		void DidCollide(ICollider player, ICollider collider);

		Int32 GetDamage();
	}
}
