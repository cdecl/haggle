﻿using Microsoft.Xna.Framework;
using System;

namespace Haggle.Core {
	public abstract class Collider: ICollider {
		private event CollisionHandler handler;

		public abstract Rectangle GetCollisionArea();

		public void RegisterHandler(CollisionHandler handler) {
			this.handler += handler;
		}

		public void DidCollide(ICollider player, ICollider collider) {
			if(handler != null)
				handler(player, collider);
		}

		public abstract Int32 GetDamage();
	}
}
