﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Haggle.Core {
	public static class Debug {
		public static void DrawRectangle(Rectangle rect, SpriteBatch spriteBatch) {
			// draw debug box
			var debug = new Texture2D(spriteBatch.GraphicsDevice, rect.Width, rect.Height);
			var data = new Color[rect.Width * rect.Height];

			for(int x = 0; x < rect.Width; ++x) {
				if(x == 0 || x == rect.Width - 1)
					for(int y = 0; y < rect.Height; ++y)
						data[x + y * rect.Width].R = 255;
				data[x].R = 255;
				data[x + (rect.Height - 1) * rect.Width].R = 255;
			}
			debug.SetData(data.Cast<Color>().ToArray());

			spriteBatch.Draw(debug, rect.Location.ToVector2());
		}
	}
}
