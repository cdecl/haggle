﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Haggle.Core {
	public abstract class Projectile: Collider, IGameObject {
		protected Texture2D sprite;
		private string sprite_name;
		protected Vector2 position;
		protected Game1 game;

		public Projectile(string sprite_name, Vector2 position, Game1 game) {
			this.sprite_name = sprite_name;
			this.position = position;
			this.game = game;

			this.RegisterHandler(collisionHandler);
		}

		public void LoadContent(ContentManager content) {
			sprite = content.Load<Texture2D>(sprite_name);
		}

		public void UnloadContent() {
			throw new NotImplementedException();
		}

		public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);
		public abstract void Update(GameTime gameTime);

		private void collisionHandler(ICollider player, ICollider collider) {
			game.collisionSystem.UnregisterCollider(this);
			game.UnregisterGameObject(this);
		}
	}
}
