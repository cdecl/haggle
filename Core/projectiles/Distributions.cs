﻿using System;
using MathNet.Numerics.Distributions;

namespace Haggle.Core {
	/// <summary>
	/// signature for a cdf function. In our case, all cdfs will have parameter
	/// t which may or may not be used.
	/// </summary>
	public delegate double cdf(int x, int t);
	public delegate int inv_cdf(double p);

	public class Distributions {

		/// <summary>
		/// Returns the cdf of a probability distribution that starts uniform, then
		/// becomes negatively skewed as t (time) increases exponentially according
		/// to alpha (set alpha = 1 for linear).
		/// </summary>
		/// <returns>The skewed cdf.</returns>
		/// <param name="alpha">Alpha.</param>
		/// <param name="n">N.</param>
		public static cdf time_skewed_cdf(double alpha, int n) {
			return (x, t) => {
				return (x + 1.0) * ((Math.Pow(t, alpha)) * (x / 2.0) + 1) / ((Math.Pow(t,alpha)) * (n * (n - 1) / 2) + n);
			};
		}

		/// <summary>
		/// Normals the cdf.
		/// </summary>
		/// <returns>The cdf.</returns>
		/// <param name="n">N.</param>
		public static inv_cdf normal_inv_cdf(int n) {
			return (p) => {
				// mean = n/2 to shift the distribution so the middle is the middle
				// of the screen.
				// stddev: Rule of thumb, ~68% 2 stddev away, so multiply n by .68,
				// divide by 2 to get stddev, then make mean the middle by
				// dividing by 2 again. 
				return (int)Normal.InvCDF(n / 2, (n * .68) / 4, p);
			};
		}

		public static inv_cdf uniform_inv_cdf(int n) {
			return (p) => {
				return DiscreteUniform.Sample(0, n);
			};
		}
	}
}
