﻿using System;
using Microsoft.Xna.Framework;

namespace Haggle.Core {
	public abstract class ProjectileForge {
		private Probability projectile_picker;
		private Probability location_picker;
		protected Game1 game;

		public ProjectileForge(Game1 game, int num_projectiles) {
			projectile_picker = Probability.get_time_skewed(num_projectiles, 1);
			location_picker = Probability.get_normal(Game1.Height);
			this.game = game;
		}

		public ProjectileForge(Game1 game, int num_projectiles, Probability location_picker) {
			projectile_picker = Probability.get_time_skewed(num_projectiles, 1);
			this.location_picker = location_picker;
			this.game = game;
		}

		/// <summary>
		/// Gets the spawn height.
		/// </summary>
		/// <returns>The location.</returns>
		protected int spawn_height(GameTime time) {
			return location_picker.sample_distribution(convert_time(time.TotalGameTime));
		}

		/// <summary>
		/// Projectiles the index. lol
		/// </summary>
		/// <returns>The index.</returns>
		protected int projectile_index(GameTime time) {
			return projectile_picker.sample_distribution(convert_time(time.TotalGameTime));
		}

		/// <summary>
		/// Spawns the projectile.
		/// </summary>
		public abstract Projectile spawn_projectile(GameTime time);

		/// <summary>
		/// Converts the TimeSpan into the correct units 
		/// (Seconds, Minutes, Half-Minutes, etc.)
		/// 
		/// Used to determine how quickly the difficulty will ramp up.
		/// </summary>
		/// <returns>The time.</returns>
		/// <param name="time">Time.</param>
		public abstract int convert_time(TimeSpan time);
	}
}
