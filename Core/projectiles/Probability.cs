﻿using System;

namespace Haggle.Core {
	public abstract class Probability {
		private Random uniform;
		private int n;

		public static Probability get_normal(int n) {
			return new InvCDFProbability(n, Distributions.normal_inv_cdf(n), 0, n);
		}

		public static Probability get_time_skewed(int n, double alpha ) {
			return new CDFSearchProbability(n, Distributions.time_skewed_cdf(alpha, n));
		}

		public static Probability get_uniform(int n) {
			return new InvCDFProbability(n, Distributions.uniform_inv_cdf(n));
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Core.Probability"/> class.
		/// Used to sample the given probability distribution.
		/// </summary>
		/// <param name="n">N. size of the domain of the distribution.</param>
		protected Probability(int n) {
			uniform = new Random();
			this.n = n;
		}

		/// <summary>
		/// <para>
		/// Samples the distribution.
		/// generate u in [0,1] using the uniform distribution,
		/// then finds x in X such that F(x) &gt;= u and
		/// F(y) &gt;= F(x) whenever F(y) &gt;= u
		/// </para>
		/// <para></para>
		/// <para>
		/// For the record, I did compute F^-1(u), but it's pretty ugly
		/// and will likely be less efficient than just scanning the list of 
		/// cdf values.
		/// </para>
		/// </summary>
		/// <para></para>
		/// <returns>The distribution.</returns>
		public int sample_distribution(int time) {
			return get_x(uniform.NextDouble(), time);
		}

		/// <summary>
		/// Gets the x value associated with the probability p, 
		/// using time parameter t.
		/// </summary>
		/// <returns>The x.</returns>
		/// <param name="p">P.</param>
		/// <param name="t">T.</param>
		protected abstract int get_x(double p, int t);
	}

	class CDFSearchProbability:Probability {
		private cdf cdf;

		/// <summary>
		/// Initializes a probability distribution. This distribution
		/// searches through cdf values to find the associated variable.
		/// </summary>
		/// <param name="n">N. size of the domain of the distribution.</param>
		/// <param name="cdf">F. cdf of the distribution.</param>
		internal CDFSearchProbability(int n, cdf cdf) : base(n) {
			this.cdf = cdf;
		}

		protected override int get_x(double p, int t) {
			// linear search is fine for now, because this class usually has n <= 4.
			return linear_search(p, t);
		}

		/// <summary>
		/// Linears the search. (Nailed it)
		/// scans the list of cdf values until it finds the index of
		/// least cdf value larger than u.
		/// </summary>
		/// <returns>The search.</returns>
		/// <param name="u">U.</param>
		int linear_search(double u, int t) {
			int curr = 0;
			while(cdf(curr, t) < u)
				curr++;
			return curr;
		}
	}

	class InvCDFProbability : Probability {
		private inv_cdf inv_cdf;
		private int min, max;

		/// <summary>
		/// Initializes a paramaterized probability distribution. This uses
		/// the inverse cdf to find the random variable.
		/// </summary>
		/// <param name="n">N. size of the domain of the distribution.</param>
		/// <param name="inv_cdf">F. inverse cdf of the distribution.</param>
		internal InvCDFProbability(int n, inv_cdf inv_cdf) : base(n) {
			this.inv_cdf = inv_cdf;
			min = int.MinValue;
			max = int.MaxValue;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Core.InvCDFProbability"/> class.
		/// Allows for min and max values for when the distribution is defined
		/// on too large of a space.
		/// </summary>
		/// <param name="n">N.</param>
		/// <param name="inv_cdf">Inv cdf.</param>
		/// <param name="min">Minimum.</param>
		/// <param name="max">Max.</param>
		internal InvCDFProbability(int n, inv_cdf inv_cdf, int min, int max) : base(n) {
			this.inv_cdf = inv_cdf;
			this.max = max;
			this.min = min;
		}

		protected override int get_x(double p, int t) {
			return Math.Min(Math.Max(inv_cdf(p), min), max);
		}
	}
}
