﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Haggle.Core {
	public class SlimeProjectileForge: ProjectileForge {
		private Wind wind;
		private const int n = 3;

		public SlimeProjectileForge(Game1 game, Wind wind) :
		base(game, n, Probability.get_uniform(Game1.GetPlayerArea().Width)) {
			this.wind = wind;
		}

		public override Projectile spawn_projectile(GameTime time) {
			float index = projectile_index(time); // determines scale of drop.
			int height = spawn_height(time); // determines where they spawn.
			var slime = new SlimeDrop(wind, (index + 1) / n,
													 height + Game1.GetPlayerArea().Left, 0, game);
			game.collisionSystem.RegisterCollider(slime);
			return slime;
		}

		public override int convert_time(TimeSpan time) {
			return time.Seconds / 15; // 1/4 minutes
		}
	}

	/// <summary>
	/// Slime Drop projectile. This projectile is fired from the Slime monster.
	/// It falls from the ceiling at varying sizes.
	/// (Should only be created by the SlimeProjectileForge.)
	/// </summary>
	class SlimeDrop: Projectile {
		public const float Speed = .5f;
		private Wind wind;
		private float rotation;
		private float scale;
		public SlimeDrop(Wind wind, float scale, int x, int y, Game1 game) :
		base("SlimeDrop", new Vector2(x, y), game) {
			this.wind = wind;
			this.rotation = 0;
			this.scale = scale;
		}

		public override void Update(GameTime gameTime) {
			Rectangle pArea = Game1.GetPlayerArea();
			if(pArea.Bottom < position.Y + sprite.Height * scale) {
				game.UnregisterGameObject(this);
				return;
			}

			// Save old position for calculating rotation angle.
			double x = position.X;
			double y = position.Y;
			position.Y += Speed;
			position += wind.force;

			// Make sure slime is pointed in right direction. (towards wind)
			rotation = (float)Math.Atan2(y - position.Y, position.X - x);
			rotation = wind.force.X > 0 ? rotation : rotation + (float)Math.PI;
			rotation = wind.force.X == 0 ? 0 : rotation;
		}

		public override void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
			Rectangle pArea = Game1.GetPlayerArea();
			if(pArea.Contains(position) && pArea.Bottom >= position.Y + sprite.Height * scale)
				spriteBatch.Draw(sprite, position,
												 rotation: rotation,
												 scale: new Vector2(scale, scale),
												 origin: sprite.Bounds.Center.ToVector2());

			//Debug.DrawRectangle(GetCollisionArea(), spriteBatch);
		}

		public override Rectangle GetCollisionArea() {
			var bounds = sprite.Bounds;

			bounds.Offset(-bounds.Center.X, -bounds.Center.Y);
			bounds.Offset(position);
			bounds.Inflate(-7, -5);
			bounds.Inflate(-0.5f * bounds.Width * (1 - scale), -0.5f * bounds.Height * (1 - scale));

			return bounds;
		}

		public override int GetDamage() {
			return (int)(scale * 200);
		}
	}
}
