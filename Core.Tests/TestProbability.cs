﻿using NUnit.Framework;
using Haggle.Core;
using System;

namespace Core.Tests {
	[TestFixture ()]
	public class TestProbability {
		private const int NUM_TRIALS = 100000; //hundred thousand
		private const double ERROR_MARGIN = .01; // Error marginfor success.
		[Test ()]
		public void TestNormal () {
			Probability normal = Probability.get_normal(100);
			long sum = 0;
			for (int i = 0; i < NUM_TRIALS; i++) {
				sum += normal.sample_distribution(0) + 1; //+1 to convert from index.
			}

			double mean = sum / (double)NUM_TRIALS;

			// Assert mean is approximately 50.5
			Assert.LessOrEqual(50.5 - mean, ERROR_MARGIN);
		}
	}
}