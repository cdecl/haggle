﻿using NUnit.Framework;
using System;
using Haggle.Core;
namespace Core.Tests {
	[TestFixture ()]
	public class TestTimeSkewed {
		private const int NUM_TRIALS = 100000; //hundred thousand
		private const int T = 10; // time steps.
		private const double ERROR_MARGIN = .01; // Error marginfor success.

		[Test ()]
		public void TestNormal () {
			Probability time_skewed = Probability.get_time_skewed (T, 1);

			// Calculate mean of distribution at each time step.
			double[] means = new double[T];

			for (int t = 0; t < T; t++) {
				long sum = 0;
				for (int i = 0; i < NUM_TRIALS; i++) {
					sum += time_skewed.sample_distribution(0) + 1; //+1 to convert from index.
				}

				means[t] = sum / (double)NUM_TRIALS;
			}

			// Assert t=0 uniform. (1 + 2 + ... + 10) / 2 = (10*11 / 2) / 10 = 5.5
			Assert.LessOrEqual(5.5 - means[0], ERROR_MARGIN);

			//Mean should get larger over time.
			for (int i = 1; i < means.Length; i++)
				Assert.Less (means[i - 1], means [i]);
		}
	}
}
